#
# - Find CppAdapter
# Find CppAdapter includes and library
#
#  CPPADAPTER_INCLUDE_DIRS - where to find Export.h, etc.
#  CPPADAPTER_LIBRARIES    - List of libraries when using WiresharkDissectorAdapter.
#  CPPADAPTER_FOUND        - True if Adapter found.
#  CPPADAPTER_DLL_DIR      - (Windows) Path to the WiresharkDissectorAdapter DLL
#  CPPADAPTER_DLL          - (Windows) Name of the WiresharkDissectorAdapter DLL
#

#set(ADAPTER_INCLUDEDIR "${WIRESHARK_BASE_DIR}/wsadapter/WiresharkDissectorAdapter/WiresharkDissectorAdapter")
#set(ADAPTER_LIBDIR "${WIRESHARK_BASE_DIR}/wsadapter/WiresharkDissectorAdapter/x64/Debug/")

set(CPPADAPTER_INCLUDEDIR "C:/Dev/wsadapter/WiresharkDissectorAdapter/WiresharkDissectorAdapter")
set(CPPADAPTER_LIBDIR "C:/Dev/wsadapter/WiresharkDissectorAdapter/x64/Debug/")

# find header file at the hinted path. If successfull, store the result in CPPADAPTER_INCLUDE_DIR
find_path(CPPADAPTER_INCLUDE_DIR
    NAMES Export.h
	HINTS "${CPPADAPTER_INCLUDEDIR}"
)

# find lib file at the hinted path
find_library(CPPADAPTER_LIBRARY
    NAMES WiresharkDissectorAdapter.lib
	HINTS "${CPPADAPTER_LIBDIR}"
)

message(STATUS "FindCPPADAPTER include dir: ${CPPADAPTER_INCLUDE_DIR} and lib dir: ${CPPADAPTER_LIBRARY}")

# check if the CPPADAPTER_LIBRARY and CPPADAPTER_INCLUDE_DIR have both been set
# and set CPPADAPTER_FOUND if that's the case
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(CPPADAPTER DEFAULT_MSG CPPADAPTER_LIBRARY CPPADAPTER_INCLUDE_DIR)

if (CPPADAPTER_FOUND)
  #include( CheckIncludeFile )
  #include( CMakePushCheckState )
  
  # set the cached values in the exported values 
  set ( CPPADAPTER_INCLUDE_DIRS ${CPPADAPTER_INCLUDE_DIR} )
  set ( CPPADAPTER_LIBRARIES ${CPPADAPTER_LIBRARY} )
  
  if (WIN32)
    # set the dll dir. In this case it coincides with the lib path
    set ( CPPADAPTER_DLL_DIR "${CPPADAPTER_LIBDIR}" 
	    CACHE PATH "PATH to WiresharkDissectorAdapter DLL"
	)	
	# set the list of dll files, matching the expression. Will be just one in this case
    file ( GLOB _cppadapter_dll RELATIVE "${CPPADAPTER_DLL_DIR}"
          "${CPPADAPTER_DLL_DIR}/WiresharkDissectorAdapter*.dll"
    )	
	# set the cached value in the exported one
    set (CPPADAPTER_DLL ${_cppadapter_dll}	
        CACHE FILEPATH "WiresharkDissectorAdapter DLL file name" 
    )
	mark_as_advanced( CPPADAPTER_DLL_DIR CPPADAPTER_DLL )
  endif()
else()
   set ( CPPADAPTER_INCLUDE_DIRS )
   set ( CPPADAPTER_LIBRARIES )
endif()  

mark_as_advanced ( CPPADAPTER_LIBRARIES CPPADAPTER_INCLUDE_DIRS )
