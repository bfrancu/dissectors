#include <epan/packet.h>   /* Should be first Wireshark include (other than config.h) */
#include <config.h>

#include <Windows.h>
#include <stdio.h>

//#include <epan/expert.h>   /* Include only as needed */
//#include <epan/prefs.h>    /* Include only as needed */

#include "Export.h"

void proto_reg_handoff_nokia_sbc(void);
void proto_register_nokia_sbc(void);

// init and cleanup routines
void nokia_sbc_init(void);
void nokia_sbc_cleanup(void);
BOOL load_adapter_module(void);
BOOL unload_adapter_module(void);


void test_tvbuffer_conversion();

/* Initialize the protocol and registered fields */
static int proto_nokia_sbc = -1;

/* Initialize the subtree pointers */
static gint ett_nokia_sbc = -1;

static dissector_handle_t ip_handle;
static dissector_handle_t udp_handle;
static dissector_handle_t rtp_handle;

/* Global sample port preference - real port preferences should generally
 * default to 0 unless there is an IANA-registered (or equivalent) port for your
 * protocol. */
#define NOKIA_SBC_UDP_PORT 3002
//static guint tcp_port_pref = NOKIA_SBC_UDP_PORT;

#define SBC_LITID_LEN 4
#define SBC_CORRID_LEN 7
#define SBC_DIRECTION_LEN 1

/* Initialize the protocol items*/
static int hf_nokia_sbc_litid = -1;
static int hf_nokia_sbc_corrid = -1;
static int hf_nokia_sbc_direction = -1;
static int hf_nokia_sbc_available_payload_size = -1;

/* file descriptor*/
static FILE *p_file = NULL;

/*pointer to the Adapter provided by the external Dll*/
static const LPCWSTR ADAPTER_DLL_NAME = L"WiresharkDissectorAdapter.dll";
static struct IDissectorAdapter * p_adapter = NULL;
static HMODULE adapter_lib_handle;

typedef struct IDissectorAdapter *(__cdecl *adapterRetrieverFuncType)(void);



static const value_string direction_value_names[] = {
	{0, "Downlink"},
	{1, "Uplink"}
};


/* A sample #define of the minimum length (in bytes) of the protocol data.
 * If data is received with fewer than this many bytes it is rejected by
 * the current dissector. */
#define NOKIA_SBC_MIN_LENGTH  12
#define IP_HEADER_MIN_LENGTH  20
#define UDP_HEADER_MIN_LENGTH 8

/* Code to actually dissect the packets */
static int dissect_nokia_sbc(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, void *data _U_)
{
    /* Set up structures needed to add the protocol subtree and manage it */
    proto_item *ti;
    proto_tree *nokia_sbc_tree;
    /* Other misc. local variables. */
    guint offset = 0;
    gint remaining_length = 0;
    char buf[50];
    guint8 direction_byte = tvb_get_guint8(tvb, 11);
    const char * payload_description = "(duplicated IP PDU to or from target)";


    /*** HEURISTICS ***/

    /* Check that the packet is long enough for it to belong to us. */
    if (tvb_reported_length(tvb) < NOKIA_SBC_MIN_LENGTH)
        return 0;	

    /*** COLUMN DATA ***/

    /* Set the Protocol column to the constant string of NOKIA_SBC */
    col_set_str(pinfo->cinfo, COL_PROTOCOL, "NOKIA_SBC");
    col_clear(pinfo->cinfo, COL_INFO);
    col_add_fstr(pinfo->cinfo, COL_INFO, "Direction: %s", val_to_str(direction_byte, direction_value_names, "Unknown %d"));
	
    /*** PROTOCOL TREE ***/

    /* create display subtree for the protocol */
    ti = proto_tree_add_item(tree, proto_nokia_sbc, tvb, 0, -1, ENC_NA);

    nokia_sbc_tree = proto_item_add_subtree(ti, ett_nokia_sbc);

    proto_tree_add_item(nokia_sbc_tree, hf_nokia_sbc_litid, tvb, offset, SBC_LITID_LEN, ENC_LITTLE_ENDIAN);
    gint32 litid = tvb_get_ntohi24(tvb, offset);

    offset += SBC_LITID_LEN;

    proto_tree_add_item(nokia_sbc_tree, hf_nokia_sbc_corrid, tvb, offset, SBC_CORRID_LEN, ENC_BIG_ENDIAN);
    gint64 corrid = tvb_get_ntoh56(tvb, offset);
    offset += SBC_CORRID_LEN;

    proto_tree_add_item(nokia_sbc_tree, hf_nokia_sbc_direction, tvb, offset, SBC_DIRECTION_LEN, ENC_BIG_ENDIAN);
    gint8 direction = tvb_get_bits8(tvb, offset, 1);
    offset += SBC_DIRECTION_LEN;

    sprintf(buf, "%s %d %llu %d %s", "\r\n", litid, corrid, direction, "/0");
    if (NULL != p_file)
    {
        //fputs(buf, p_file);
    }

    remaining_length = tvb_captured_length_remaining(tvb, offset);
    proto_tree_add_string_format_value(nokia_sbc_tree, hf_nokia_sbc_available_payload_size, tvb,
        offset, -1, "", "%d %s %s", remaining_length, "bytes", payload_description);

    // handle ip header

    // use this line to process the buffer only for the length of ip header
    // tvbuff_t* next_tvb = tvb_new_subset_length_caplen(tvb, offset, IP_HEADER_MIN_LENGTH, IP_HEADER_MIN_LENGTH);

    // or use either this line to process the rest of remaining buffer
    tvbuff_t* next_tvb = tvb_new_subset_length_caplen(tvb, offset, remaining_length, remaining_length);
    offset += IP_HEADER_MIN_LENGTH;

    call_dissector(ip_handle, next_tvb, pinfo, tree);

    // handle udp header

    //next_tvb = tvb_new_subset_length_caplen(tvb, offset, UDP_HEADER_MIN_LENGTH, UDP_HEADER_MIN_LENGTH);

    //remaining_length = tvb_captured_length_remaining(tvb, offset);
    //next_tvb = tvb_new_subset_length_caplen(tvb, offset, remaining_length, remaining_length);

    offset += UDP_HEADER_MIN_LENGTH;
    //call_dissector(udp_handle, next_tvb, pinfo, tree);

    // handle rtp header + payload
    //next_tvb = tvb_new_subset_length(tvb, offset, remaining_length);
    remaining_length = tvb_captured_length_remaining(tvb, offset);
    next_tvb = tvb_new_subset_length_caplen(tvb, offset, remaining_length, remaining_length);

    call_dissector(rtp_handle, next_tvb, pinfo, tree);

    return tvb_captured_length(tvb);
}


/* Register the protocol with Wireshark.
 *
 * This format is require because a script is used to build the C function that
 * calls all the protocol registration.
 */
void proto_register_nokia_sbc(void)
{
    /* Setup list of header fields  See Section 1.5 of README.dissector for
     * details. */
    static hf_register_info hf[] = {
        { &hf_nokia_sbc_litid,
          { "LITID", "nokia_sbc.litid",
            FT_UINT32, BASE_DEC,
                        NULL, 0x0,
            NULL, HFILL }
        },
                { &hf_nokia_sbc_corrid,
                  { "CorrID", "nokia_sbc.corrid",
                     FT_UINT56, BASE_DEC,
                     NULL, 0x0,
                     NULL, HFILL }
                },
                { &hf_nokia_sbc_direction,
                   { "Direction", "nokia_sbc.direction",
                      FT_UINT8, BASE_DEC,
                          VALS(direction_value_names), 0x0,
                          NULL, HFILL }
                },
                { &hf_nokia_sbc_available_payload_size,
                    { "Available Payload", "nokia_sbc.payload",
                           FT_STRING, BASE_NONE,
                           NULL, 0x0,
               NULL, HFILL }
                }
    };

    /* Setup protocol subtree array */
    static gint *ett[] = { &ett_nokia_sbc };

	/* Register the protocol name and description */
    proto_nokia_sbc = proto_register_protocol("NOKIA SBC V18", "SBCV18", "nokia_sbc");

	/* Required function calls to register the header fields and subtrees */
    proto_register_field_array(proto_nokia_sbc, hf, array_length(hf));
    proto_register_subtree_array(ett, array_length(ett));
}


/* Simpler form of proto_reg_handoff_NOKIA_SBC which can be used if there are
 * no prefs-dependent registration function calls. */
void proto_reg_handoff_nokia_sbc(void)
{
    static dissector_handle_t nokia_sbc_handle;
	
    register_init_routine(nokia_sbc_init);
    register_cleanup_routine(nokia_sbc_cleanup);

    nokia_sbc_handle = create_dissector_handle(dissect_nokia_sbc, proto_nokia_sbc);
    dissector_add_uint("udp.port", NOKIA_SBC_UDP_PORT, nokia_sbc_handle);

    ip_handle = find_dissector("ip");
    udp_handle = find_dissector("udp");
    rtp_handle = find_dissector("rtp");
}

void nokia_sbc_init(void)
{
	p_file = fopen ("c:\\Dev\\wsbuild64\\plugins\\epan\\nokia_sbc\\sbc_logs.txt", "w+");

	if (NULL != p_file)
	{
		fputs("nokia_sbc_init", p_file);
		fflush(p_file);
	}
        //p_adapter = GetAdapter();


        if (0 == load_adapter_module())
        {
            fputs("\nAdapter module not loaded", p_file);
        }

        if (NULL != p_adapter)
        {
            confirmAdapter(p_adapter);
            fputs("\nAdapter is not NULL", p_file);
        }
        else
        {
            fputs("\nAdapter is NULL", p_file);
        }
}

void nokia_sbc_cleanup(void)
{
    if (NULL != adapter_lib_handle)
    {
        if (NULL != p_adapter)
        {
            fputs("\nReleasing Adapter", p_file);
            releaseAdapter(p_adapter);
        }
        else
        {
            fputs("\nAdapter is null. No release needed", p_file);
        }
        unload_adapter_module();
    }

    if (NULL != p_file)
    {
	fputs("\nnokia_sbc_cleanup", p_file);
	fclose(p_file);
    }

}

BOOL load_adapter_module(void)
{
    BOOL result = 0;
    adapterRetrieverFuncType p_retriever_addr;

    adapter_lib_handle = LoadLibrary(ADAPTER_DLL_NAME);

    if (NULL != adapter_lib_handle)
    {
        fputs("\nAdapter module loaded", p_file);
        p_retriever_addr = (adapterRetrieverFuncType) GetProcAddress(adapter_lib_handle, "GetAdapter");
        if (NULL != p_retriever_addr)
        {
            fputs("\nProcess address of \"GetAdapter()\" retrieved", p_file);
            p_adapter = p_retriever_addr();
            if (NULL != p_adapter)
            {
                fputs("\n", p_file);
                result = 1;
            }
            else
            {
                /*loading failed*/
                FreeLibrary(adapter_lib_handle);
            }
        }
        else
        {
            fputs("\nCouldn't retrieve the process adddress of \"GetAdapter()\"", p_file);
        }
    }
    else
    {
        fputs("\nAdapter module loading failed", p_file);
    }

    return result;
}

BOOL unload_adapter_module(void)
{
    BOOL result = 0;
    if (NULL != adapter_lib_handle)
    {
        result = FreeLibrary(adapter_lib_handle);
    }
    return result;
}
