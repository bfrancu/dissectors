//#include "stdafx.h"
#include "DissectorLogger.h"
#include "Export.h"

IDissectorAdapter *__cdecl GetAdapter()
{
	return new DissectorLogger;
}

void _cdecl confirmAdapter(struct IDissectorAdapter* p_adapter)
{
	DissectorLogger * p_logger = static_cast<DissectorLogger*> (p_adapter);
	p_logger->log();	
}


void __cdecl releaseAdapter(struct IDissectorAdapter * p_adapter)
{
	p_adapter->release();
}

DissectorLogger::DissectorLogger() :
	m_file_stream{ "c:\\Dev\\wsadapter\\WiresharkDissectorAdapter\\logs.txt", std::fstream::out}
{
	m_file_stream << "DissectorLogger::init()\n";
}


DissectorLogger::~DissectorLogger()
{
}

void DissectorLogger::log()
{
	m_file_stream << "DissectorLogger::log() called from Dissector code\n";
}

void DissectorLogger::dissect(uint8_t * p_buffer, uint32_t buffer_length)
{
	m_file_stream << "DissectorLogger::dissect()\n";
}

void DissectorLogger::release()
{
	m_file_stream << "DissectorLogger::release()\n";
	m_file_stream.close();
	delete this;
}
