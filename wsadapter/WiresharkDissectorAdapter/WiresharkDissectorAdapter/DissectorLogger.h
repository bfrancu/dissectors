#pragma once
#include  <fstream>

#include "IDissectorAdapter.h"

#ifndef WS_ADAPTER_EXPORTS 
#define WS_ADAPTER_EXPORTS 1
#endif

class DissectorLogger : public IDissectorAdapter
{
public:
	DissectorLogger();
	~DissectorLogger();

public:
	void log();

public:
	void dissect(uint8_t *p_buffer, uint32_t buffer_length) override;
	void release() override;

private:
	std::fstream m_file_stream;
};

