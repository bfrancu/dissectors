#pragma once

struct IDissectorAdapter;

#ifdef WS_ADAPTER_EXPORTS
#define ADAPTER_EXPORT __declspec(dllexport)
#else
#define ADAPTER_EXPORT __declspec(dllimport)
#endif

#ifdef __cplusplus
extern "C"
#endif
ADAPTER_EXPORT struct IDissectorAdapter *__cdecl GetAdapter();

#ifdef __cplusplus
extern "C"
#endif
ADAPTER_EXPORT void __cdecl releaseAdapter(struct IDissectorAdapter * p_adapter);


#ifdef __cplusplus
extern "C"
#endif
ADAPTER_EXPORT void __cdecl confirmAdapter(struct IDissectorAdapter * p_adapter);