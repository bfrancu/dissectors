#pragma once
#include <stdint.h>
//#include <windef.h>

class IDissectorAdapter
{
protected:
	virtual ~IDissectorAdapter() {};

public:	
	virtual void dissect(uint8_t *p_buffer, uint32_t buffer_length) = 0;
	virtual void release() = 0;
};

